from scapy.all import sniff, send, IP, UDP, Ether, Raw, AsyncSniffer
from threading import Thread, Event
import time
import pickle
import sys
import socket
import requests

HELLO_PORT = 7777
BROADCAST_ADDRESS = '11.255.255.255'
BROADCAST_FILTER = "udp and host " + BROADCAST_ADDRESS
BROADCAST_INTERFACE = "ap1-mp2"   #"h1-eth0"
DISCOVERY_SERVER_UPDATE_INTERVAL = 2

neighbours = {}
neighbours_updated = False
node_ip_address = ""


def pretty_print_dict(adict):
    for i in adict:
        print (i, adict[i])

# handler thread for sniffing
# grab the hello messages and update our neighbours dictionary (table)
def udp_receive(pkt):
    global neighbours_updated
    global neighbours
    print("in udp recv")
    pkt.show()
    if pkt[UDP].dport == HELLO_PORT :
        mac = pkt.src
        ip_src = pkt[IP].src
        ip_dst = '11.0.0.1'
        neighbours_updated = True
        neighbours = {'node1':ip_src,'time':time.time(),'node2':ip_dst,'cost':1}
        
        ###
        print (10*"-" + " Neighbour Discovery Table " + 10*"-")
        pretty_print_dict(neighbours)       
        if neighbours_updated == True:
            print ("  --> sending updated Neighbour Table")
            neighbours_updated = False # reset as will send current neighbour table to discovery server 
            end_point = 'http://10.0.0.1:5000/update_topo'
            try: 
                r = requests.post(url=end_point,data=neighbours,verify=False)
                rdata=r.text
                print ("  --> Server response: ", rdata)
            except :
                print (sys.exc_info()[0])
            time.sleep(DISCOVERY_SERVER_UPDATE_INTERVAL)

    else:
        print ("other port")
        #show(pkt)


def main(): 

    global neighbours_updated 
    neighbours_updated = False
    #node_ip_address = get_ip_address()
    node_ip_address = '11.0.1.1'
    try:   
        sniff(iface = BROADCAST_INTERFACE, prn=udp_receive, filter = BROADCAST_FILTER, store=False)
    except:
        print("sniff ")
        # background sniff ancd call handler udp_receive to process received frames.
    try:
        t = AsyncSniffer(iface=BROADCAST_INTERFACE, prn=udp_receive, filter = BROADCAST_FILTER, store=False)
        # start the background sniffer thread
        t.start()
    except:
        print("tttt")
        #sys.exit()

if __name__ == '__main__':

    main()




