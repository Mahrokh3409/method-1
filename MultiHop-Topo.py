#!/usr/bin/python

'Setting position of the nodes'

import sys

from mininet.log import setLogLevel, info
from mn_wifi.cli import CLI
from mn_wifi.node import UserAP
from mininet.node import RemoteController, Controller
from mn_wifi.net import Mininet_wifi
from mn_wifi.link import wmediumd, mesh
from mn_wifi.wmediumdConnector import interference
from mininet.term import makeTerm 

class InbandController( RemoteController ):

    def checkListening( self ):
        "Overridden to do nothing."
        return


def topology():

    net = Mininet_wifi(link='wmediumd', wmediumd_mode='interference')
   
    info("*** Creating nodes\n")



    ap1 = net.addAccessPoint('ap1', wlans=2, ssid='new-ssid', mode='g', channel='1', ip="10.0.0.11/8",range=[100,20],
                              position='10,10,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap2 = net.addAccessPoint('ap2', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.12/8",range=[100,20],
                             position='25,10,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap3 = net.addAccessPoint('ap3', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.13/8",range=[100,20],
                             position='40,10,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)

    ap4 = net.addAccessPoint('ap4', wlans=2, ssid='new-ssid', mode='g', channel='1', ip="10.0.0.14/8",range=[100,20],
                              position='55,10,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap5 = net.addAccessPoint('ap5', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.15/8",range=[100,20],
                             position='10,25,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    '''
    ap6 = net.addAccessPoint('ap6', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.6/8",range=[100,20],
                             position='40,25,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap7 = net.addAccessPoint('ap7', wlans=2, ssid='new-ssid', mode='g', channel='1', ip="10.0.0.7/8",range=[100,20],
                              position='55,25,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap8 = net.addAccessPoint('ap8', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.8/8",range=[100,20],
                             position='10,40,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap9 = net.addAccessPoint('ap9', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.9/8",range=[100,20],
                             position='40,40,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    ap10 = net.addAccessPoint('ap10', wlans=2, ssid='new-ssid',  mode='g', channel='1', ip="10.0.0.10/8",range=[100,20],
                             position='55,40,0', inband=True, 
                             inNamespace=True, cls=UserAP, client_isolation=True)
    '''

    sta1= net.addStation('sta1', ip='10.0.0.1/8',position='30,30,0', range=100)
    c1 = net.addController('c1', ip="10.0.0.1", controller=InbandController, port=6653)

    info("*** Configuring propagation model\n")
    net.setPropagationModel(model="logDistance", exp=4.5)

    info("*** Configuring wifi nodes\n")
    net.configureWifiNodes()

    info("*** Creating links\n")
    for i in range(1,6):
	net.addLink(net.get('ap{}'.format(i)), intf='ap{}-wlan2'.format(i), ssid='mesh-ssid', cls=mesh, channel=5)

    
    
    sta1.setMasterMode(intf='sta1-wlan0', ssid='new-ssid', channel='1', mode='g')
    for i in range(1,6):
      net.get('ap{}'.format(i)).setManagedMode(intf='ap{}-wlan1'.format(i))

    for i in range(1,6):
      net.get('ap{}'.format(i)).cmd('iw dev ap{}-wlan1 connect new-ssid'.format(i))


 
    #net.plotGraph(max_x=70, max_y=70)
    info("*** Starting network\n")
    net.build()
    c1.start()
    for i in range(1,6):
	net.get('ap{}'.format(i)).start([c1])


    for i in range(1,6):
        net.get('ap{}'.format(i)).wintfs[1].setIP('11.0.0.{}/8'.format(i))


    for i in range(1,6):
        net.get('ap{}'.format(i)).wintfs[0].setIP('10.0.0.{}/8'.format((i*10)+1))

    for i in range(1,6):
      net.get('ap{}'.format(i)).cmd('iw dev ap{}-wlan1 connect new-ssid'.format(i))

    info("*** Running CLI\n")
    CLI(net)

    info("*** Stopping network\n")
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    topology()
