# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from ryu.base import app_manager
from operator import attrgetter
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib import hub
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet,ipv4,tcp,udp
from ryu.lib.packet import ether_types
from ryu.ofproto import ofproto_v1_3_parser as p13
import datetime
import random
from sortedcontainers import SortedDict as sd
import numpy
import sys
import requests
from copy import deepcopy

class Router(app_manager.RyuApp):
  OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
  def __init__(self, *args, **kwargs):
    super(Router, self).__init__(*args, **kwargs)
    self.datapaths = {}
    self.hosts = {'00:00:00:00:00:01':[1,1],'00:00:00:00:00:02':[2,1],'00:00:00:00:00:03':[3,1],'00:00:00:00:00:04':[4,1]} # {macaddr1:[dpid1,pr1],macaddr2:[dpid2,pr2]...} hardcoding it so I dont have to handle ARP
	   
  @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
  def switch_features_handler(self, ev):
    datapath = ev.msg.datapath
    ofproto = datapath.ofproto
    parser = datapath.ofproto_parser
    match = parser.OFPMatch()
    actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
    self.datapaths[datapath.id]=datapath
    inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,actions)] # modification instructions
    mod = parser.OFPFlowMod(datapath = datapath, priority = 0, match= match, instructions= inst, hard_timeout= 0) # flow_mod packet
    datapath.send_msg(mod)

  @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
  def _packet_in_handler(self, ev): # to find a path with max abw to provision. # new packets
    msg = ev.msg
    datapath = msg.datapath
    ofproto = datapath.ofproto
    dpid = datapath.id
    parser = datapath.ofproto_parser
    in_port = msg.match['in_port']
    pkt = packet.Packet(msg.data)
    eth = pkt.get_protocols(ethernet.ethernet)[0]
    ippkt = pkt.__contains__(ipv4.ipv4)
    dst_mac = eth.dst
    src_mac = eth.src
    if ippkt:
      ipp = pkt.get_protocols(ipv4.ipv4)[0]
      src_ip = ipp.src
      dst_ip = ipp.dst
      if ipp.proto == 6: #tcp
        tcpp = pkt.get_protocols(tcp.tcp)[0]
        src_pr = tcpp.src_port
        dst_pr = tcpp.dst_port
        ipproto = 6
      elif ipp.proto == 17: # udp
        udpp = pkt.get_protocols(udp.udp)[0]
        src_pr = udpp.src_port
        dst_pr = udpp.dst_port
        ipproto = 17
      else:
        src_pr = 0
        dst_pr = 0
        ipproto=0# need to check the value of this feild.
      f = (src_mac,dst_mac,src_ip,dst_ip) # flow

    self.logger.info("packet in %s %s %s %s", dpid, src_mac, dst_mac, in_port)
    #self.hosts[src_mac] = [dpid,in_port] ### use ARP to build.
    if eth.ethertype == ether_types.ETH_TYPE_LLDP:
      pass
    if src_mac not in self.hosts.keys(): # if dpid not in hosts register
      self.hosts.setdefault(src_mac,[0,0])
    # find paths between src and dst.
    if dst_mac in self.hosts.keys(): # if src and dst are same
      src_dpid = self.hosts[src_mac][0] # find src switch
      dst_dpid = self.hosts[dst_mac][0] # find dst switch

      path = self.find_path(src_dpid,dst_dpid)# update paths
      print(path)
      self.configure_flows(path,dst_mac,pkt,parser)


  def find_path(self,src_dpid,dst_dpid):
    url = 'http://localhost:5000/get_path/src_dpid/dst_dpid'
    resp = requests.get(url)
    print resp
    return path

  def configure_flows(self,path,dst_mac,pkt,parser): # on a known path, configure flow on all the devices
    # develop end to end part , by stringin sw and ports together
    if len(path) == 1: # if src and dst are same
      out_port = self.hosts[dst_mac][1]
      actions = [parser.OFPActionOutput(out_port)]
      match = self.match_simple(pkt,parser)
      self.add_flow(self.datapaths[path[0]], 1, match, actions)
    else:
      for i in range(len(path)): # install the flow on all the switches in the path
        match = self.to_match(pkt,parser) #extended match criteria
        #determine outport
        if path[i] == path[-1]: # if cur node is destination node, then outport is port conecting to the host
          out_port = self.hosts[dst_mac][1]
        else:
          out_port = self.topology[path[i]][path[i+1]][0] # port in wifi is a frequency
        #compose actions
        actions = [parser.OFPActionOutput(out_port)]
        #add flow rules to a datapath
        self.add_flow(self.datapaths[path[i]], 1, match, actions)

  def to_match(self,pkt,parser):
    eth = pkt.get_protocols(ethernet.ethernet)[0]
    ippkt = pkt.__contains__(ipv4.ipv4)
    dst_mac = eth.dst
    src_mac = eth.src
    if ippkt: # for now we are focusing only on ippkt. # to install fine grain flows.this will also allow testing of how load changes with background traffic
      ipp = pkt.get_protocols(ipv4.ipv4)[0]
      match = parser.OFPMatch(eth_dst=dst_mac,eth_src=src_mac,eth_type=ether_types.ETH_TYPE_IP,ip_proto=ipp.proto,ipv4_dst=ipp.dst,ipv4_src=ipp.src)
    else:  # if not ippkt, the use dst_mac addresses
      match = parser.OFPMatch(eth_dst=dst_mac)
    return match

  def add_flow(self, datapath, priority, match, actions): #update to add hard-timeout
    #print("adding flows on"+str(datapath.id))
    ofproto = datapath.ofproto
    parser = datapath.ofproto_parser
    inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,actions)] # modification instructions
    mod = parser.OFPFlowMod(datapath = datapath, priority = priority, match= match, instructions= inst,hard_timeout= 0) # flow_mod packet
    datapath.send_msg(mod)

  def delete_flow(self,datapath, match, out_port):
    #print("deleting flows on"+str(datapath.id))
    ofproto = datapath.ofproto
    parser = datapath.ofproto_parser
    mod = parser.OFPFlowMod(datapath=datapath,command=ofproto.OFPFC_DELETE,out_port=out_port,out_group=ofproto.OFPG_ANY,priority=1,match=match)
    #print mod
    datapath.send_msg(mod)

  def remove_flows(self,path,dst_mac,pkt):
    # develop end to end part , by stringin sw and ports together
    if len(path) == 1: # if src and dst are same
      datapath = self.datapaths[path[0]]
      parser = datapath.ofproto_parser
      out_port = self.hosts[dst_mac][1]
      match = parser.OFPMatch(eth_dst=dst_mac)
      self.delete_flow(datapath, match, out_port)
    else:
      for i in range(len(path)): # install the flow on all the switches in the path
        #determine outport
        if path[i] == path[-1]: # if cur node is destination node, then outport is port conecting to the host
          out_port = self.hosts[dst_mac][1]
        else:
          out_port = self.topology[path[i]][path[i+1]][0] # outport is the port connecting to the next hop
        #compose actions
        datapath = self.datapaths[path[i]]
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_dst=dst_mac)
        self.delete_flow(datapath, match, out_port)

