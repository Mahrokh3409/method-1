from scapy.all import sniff, send, IP, UDP, Ether, Raw, AsyncSniffer
from threading import Thread, Event
import time
import pickle
import zmq
import sys

HELLO_PORT = 7777
BROADCAST_ADDRESS = '11.255.255.255'
BROADCAST_FILTER = "udp and host " + BROADCAST_ADDRESS
BROADCAST_INTERFACE = "ap1-mp2"   #"h1-eth0"
DISCOVERY_SERVER = "10.0.0.1:9996"
DISCOVERY_SERVER_UPDATE_INTERVAL = 2


neighbours = {}
neighbours_updated = False
node_ip_address = ""


import socket
def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def pretty_print_dict(adict):
    for i in adict:
        print (i, adict[i])

# handler thread for sniffing
# grab the hello messages and update our neighbours dictionary (table)
def udp_receive(pkt):
    global neighbours_updated
    if pkt[UDP].dport == HELLO_PORT :
        #print 15 * "-"
        #print "hello received"
        #pkt.show()
        mac = pkt.src
        ip_src = pkt[IP].src
        timestamp = time.time()
        #neighbours[ip_src] = (mac, timestamp)
        neighbours[ip_src] = timestamp
        neighbours_updated = True #lazy way to check the table has been updated.
        #print timestamp, mac, ip_src

    else:
        print ("other port")
        #show(pkt)


def main(): 

    global neighbours_updated 
    neighbours_updated = False
    #node_ip_address = get_ip_address()
    node_ip_address = '11.0.1.1'
    try:
        #Connect to ZMQ Discovery Server
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://" + DISCOVERY_SERVER)
        print ("connected to discovery server")
    except:
       pass
       print("cannot connect to server")
    try:

        sniff(iface = BROADCAST_INTERFACE, prn=udp_receive, filter = BROADCAST_FILTER, store=False)
        # background sniff ancd call handler udp_receive to process received frames.
        t = AsyncSniffer(iface=BROADCAST_INTERFACE, prn=udp_receive, filter = BROADCAST_FILTER, store=False)
              

        # start the background sniffer thread
        t.start()
    except:
        sys.exit()

    # Periodically send the the neighbour table to the SDN controller app.
    try:
        while True:
            print (10*"-" + " Neighbour Discovery Table " + 10*"-")
            pretty_print_dict(neighbours)       
       
            #send Neighbour table to ZMQ Discovery Server
            if neighbours_updated == True:
                neighbours_hash = {}
                print ("  --> sending updated Neighbour Table")
                neighbours_updated = False # reset as will send current neighbour table to discovery server 

                # To reduce messages to server. Send the current nodes IP address as part of a dictionary.
                # {node_ip_address : {neighbours dictionary} }
                neighbours_hash = {node_ip_address : neighbours}
                msg = pickle.dumps(neighbours_hash) #pickle the dictionary to send over network
                # using REQ/REPL zmq model. Send something, then wait for a reply. Blocking.                
                socket.send(msg)  # send msg
                response = socket.recv()  # wait for REPLY. Block until a reply comes back!!!
                print ("  --> Server response: ", response)
                
            time.sleep(DISCOVERY_SERVER_UPDATE_INTERVAL)

    except KeyboardInterrupt:
        # quit
        #send message to server that client's ndisc closing down
        #socket.send("BYE") ## maybe pickle this.
        t.stop()
        sys.exit()




   
if __name__ == '__main__':

    main()




