from flask import Flask,request
import json
import time
from SPF import shortestpath

app = Flask(__name__)
topology = {}
#G = {'a':{'b':2,'c':4,'d':7},'b':{'a':2,'c':3,'d':4},'c':{'d':2,'b':3,'a':4},'d':{'c':2,'a':7,'b':4}}
G={}

@app.route("/update_topo",methods=['GET','POST'])
def update_topo():
    node1 = request.form.get('node1',None)
    node2 = request.form.get('node2',None)
    time = request.form.get('time',None)
    cost = request.form.get('cost',None)
    if node1 != node2:
        if (node1 in topology.keys()):
            topology[node1][node2]=[time,cost]
            G[node1][node2]=int(cost)
        else:
            topology[node1]={node2:[time,cost]}
            G[node1]={node2:int(cost)}
    return json.dumps(topology)

@app.route("/get_path",methods=['GET','POST'])
def spf():
    src = request.form.get('src',None)
    dst = request.form.get('dst',None)
    print("finding route between "+str(src)+str(dst))
    if src not in G.keys() or dst not in G.keys():
        path = '0'#this is important as rest doesnt seem to like None, False or empty return string
    else:
    	path=shortestpath(G,src,dst)
    	print ("route between" +str(src)+str(dst)+ "is"+str(path) )
    return json.dumps(path)

@app.route("/get_topo",methods=['GET','POST'])
def topo():
    print("topology-->" + str(topology))
    return json.dumps(topology)

if __name__=='__main__':
    app.run(debug=True,host='0.0.0.0')
