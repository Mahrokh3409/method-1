#from scapy.all import sniff, send, IP, UDP, Ether, Raw
from scapy.all import sniff, send, IP, UDP, Ether, Raw
import time
import sys

HELLO_PERIOD = 2
HELLO_PORT = 7777
BROADCAST_ADDRESS = '11.255.255.255'
BROADCAST_INTERFACE = "ap2-mp2" #""  h2-eth0

def hello(dst_addr, port, data):
    # the src address of node sending hello message is automatically set to the nodes real IP address.
    send(IP(dst=dst_addr)/UDP(dport=port)/Raw(load=data), verbose=False)    
        
def main():
    try:
        while True:
            hello(BROADCAST_ADDRESS, HELLO_PORT,"hello")
            print ("sending hello @ ", time.time())
            time.sleep(HELLO_PERIOD)

    except KeyboardInterrupt:
        # quit
        sys.exit()

if __name__ == '__main__':
   
    main()

   
